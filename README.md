# Zabbix-Allnet

## Overview

This template monitored all (real) Sensors from you Allnet-Server

## Requirements (Allnet-Server)
- Configuration > Server and users > Access controll > Activate remote controll > Enabled
- Configuration > Server and users > Server settings > Operation mode with/without SSL > https://

## Tested with
|Model|Software-Version|Patch|
|-----|----------------|-----|
|AL3419|3.35|1106|

## Author

Christoph Murken

## Macros used

|Name|Description|Default|Type|
|----|-----------|-------|----|
|{HOST.CONN}|<p>-</p>|<i>DNS or IP of the Host</i>|Text|


## Template links

ICMP Ping

## Items

|Name|Description|Type|Key and additional info|
|----|-----------|----|----|
|AllnetSensorData|<p>-</p>|`HTTP agent`|Allnet.SensorData<p>Update: 5m</p>|

## Discovery rules

|Name|Key|Type|LLD macros|Filters|
|----|---|----|----------|-------|
|AllnetSensorData|AllnetSensorData|Dependent Item (`Allnet.SensorData`)|`{#ALLNET.SENSOR.ID}`<br>`{#ALLNET.SENSOR.NAME}`<br>`{#ALLNET.SENSOR.NAME}`<br>`{#ALLNET.SENSOR.INFO.ACTIV}`<br>`{#ALLNET.SENSOR.INFO.ENABLED}`<br>`{#ALLNET.SENSOR.INFO.UNIT}`<br>`{#ALLNET.SENSOR.CONFIG.LIMIT.MIN}`<br>`{#ALLNET.SENSOR.CONFIG.LIMIT.MAX}`<br>`{#ALLNET.SENSOR.CONFIG.DISPLAY.MIN}`<br>`{#ALLNET.SENSOR.CONFIG.DISPLAY.MAX}`|`{#ALLNET.SENSOR.ID}` does not match `^9[678]$` and<br>`{#ALLNET.SENSOR.INFO.ACTIV}` match `1` and<br>`{#ALLNET.SENSOR.INFO.ENABLED}` match `1`<br>|

## Item prototypes
|Name|Key|Type|Type fo Information|
|----|---|----|-------------------|
|Sensor '{#ALLNET.SENSOR.NAME}'|Allnet.SensorData.value[{#ALLNET.SENSOR.ID}]|Dependent Item|float|

## Trigger prototypes
|Name|Severity|Description|
|----|--------|----------|
|{#ALLNET.SENSOR.NAME}: Out of Range ({#ALLNET.SENSOR.CONFIG.LIMIT.MIN}{#ALLNET.SENSOR.INFO.UNIT} - {#ALLNET.SENSOR.CONFIG.LIMIT.MAX}{#ALLNET.SENSOR.INFO.UNIT})|High|Sensor value out of Range.<br>Limit defined by Allnet-Server|

## Graph prototypes
|Name|Items|Additional info|
|----|-----|---------------|
|Sensor '{#ALLNET.SENSOR.NAME}'|ALLNET: Sensor '{#ALLNET.SENSOR.NAME}'|percentile 90%|